from flask import Blueprint
from flask_restful import Api
import resources.tree


api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Route
api.add_resource(resources.tree.GetTree, '/')
api.add_resource(resources.tree.AddItem, '/')
api.add_resource(resources.tree.DeleteItem, '/item/<int:id>')
api.add_resource(resources.tree.MoveItem, '/item/<int:id>')
