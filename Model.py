from marshmallow import fields
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.orm.collections import attribute_mapped_collection


ma = Marshmallow()
db = SQLAlchemy()


class Item(db.Model):
    __tablename__ = 'item'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    parent_id = db.Column(db.Integer, ForeignKey('item.id'))

    children = relationship(
        "Item",
        # cascade deletions
        cascade="all, delete-orphan",
        # many to one + adjacency list - remote_side
        # is required to reference the 'remote'
        # column in the join condition.
        backref=backref("parent", remote_side=id),
        # children will be represented as a dictionary
        # on the "name" attribute.
        collection_class=attribute_mapped_collection("name"),
    )

    def __init__(self, name, parent_id=None):
        self.name = name
        self.parent_id = parent_id

    def __repr__(self):
        # return '<User %r>' % self.name
        return '' % {
            "name" : self.name,
            "id" : self.id,
            "parent_id" : self.parent_id
        }


class AddSchema(ma.Schema):
    name = fields.String(required=True)
    parent_id = fields.Integer(required=True)


class PatchSchema(ma.Schema):
    name = fields.String(required=True)
    parent_id = fields.Integer(required=True)
