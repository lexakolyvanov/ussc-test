from flask_restful import Resource
import Model
from flask import jsonify, request, make_response
from Model import db
import re

def list_to_tree(data):
    out = {
    }
    for p in data:
        if p['parent_id'] == None:
            p['parent_id'] = 0
        pid = p['parent_id'] or 'root'
        out.setdefault(pid, { 'childs': [] })
        out.setdefault(p['id'], { 'childs': [] })
        out[p['id']].update(p)
        out[pid]['childs'].append(out[p['id']])
    return out['root']['childs']


class GetTree(Resource):
    def get(self):
        items = Model.Item.query.all()
        if items == []:
            response = make_response(jsonify(items), 200)
            response.headers['Content-Type'] = 'application/json'
            return response

        response_data = []
        for item in items:
            response_data.append(dict(id=item.id, name=item.name, parent_id=item.parent_id))
        response_data = list_to_tree(response_data)
        response = make_response(jsonify(response_data), 200)
        response.headers['Content-Type'] = 'application/json'
        return response


addSchema = Model.AddSchema()


class AddItem(Resource):
    def post(self):
        req_data = request.get_json(force=True)
        if not req_data:
            return {'error': 'No input data provided'}, 400

        data, err = addSchema.load(req_data)
        if err:
            return err, 400

        valid_name = re.fullmatch(r'^[a-zA-Z0-9_а-яА-я]+( [a-zA-Z0-9_а-яА-я]+)*$', data['name'])
        if valid_name is None:
            return {'error': 'Name is not valid'}

        if data['parent_id'] == 0:
            items = Model.Item.query.filter_by(name=data['name'], parent_id=None).first()
            if items:
                return {'error': 'Item with name ' + data['name'] + ' is exist'}
            item = Model.Item(name=data['name'])
        else:
            items = Model.Item.query.filter_by(name=data['name'], parent_id=data['parent_id']).first()
            if items:
                return {'error': 'Item with name ' + data['name'] + ' is exist'}
            items = Model.Item.query.filter_by(id=data['parent_id']).first()
            if items is None:
                return {'error':'Item with parent_id ' + str(data['parent_id']) + ' not found'}
            item = Model.Item(name=data['name'], parent_id=data['parent_id'])

        db.session.add(item)
        db.session.commit()
        response = make_response(jsonify({'OK': True}), 201)
        response.headers['Content-Type'] = 'application/json'
        return response


patchSchema = Model.PatchSchema()


class MoveItem(Resource):
    def put(self, id):
        req_data = request.get_json(force=True)
        if not req_data:
            return {'error': 'No input data provided'}, 400
        data, err = patchSchema.load(req_data)
        if err:
            return err, 400

        valid_name = re.fullmatch(r'^[a-zA-Z0-9_а-яА-я]+( [a-zA-Z0-9_а-яА-я]+)*$', data['name'])
        if valid_name is None:
            return {'error': 'Name is not valid'}

        items = Model.Item.query.filter_by(id=id).first()
        if data['parent_id'] == 0:
            check_items = Model.Item.query.filter_by(name=data['name'], parent_id=None).first()
            if check_items:
                return {'error': 'Item with name ' + data['name'] + ' is exist'}
            items.parent_id = None
            items.name = data['name']
        else:
            parent = Model.Item.query.filter_by(id=data['parent_id']).first()
            check_items = Model.Item.query.filter_by(name=data['name'], parent_id=data['parent_id']).first()
            if check_items:
                return {'error': 'Item with name ' + data['name'] + ' is exist'}
            if parent is None:
                return {'error':'Item with parent_id ' + str(data['parent_id']) + ' not found'}
            items.parent_id = data['parent_id']
            items.name = data['name']
        db.session.commit()

        response = make_response(jsonify({'OK': True}), 201)
        response.headers['Content-Type'] = 'application/json'
        return response


def get_children(out, data, parent_id):
    for child in data:
        if child['parent_id'] == parent_id:
            out.append(child['id'])
            get_children(out, data, child['id'])
    return out


class DeleteItem(Resource):
    def delete(self, id):
        items = Model.Item.query.filter_by(id=id).first()
        if items is None:
            return {'error': 'Item with id ' + str(id) + ' not found'}

        items = Model.Item.query.all()
        response_data = []
        for item in items:
            response_data.append(dict(id=item.id, name=item.name, parent_id=item.parent_id))

        branch = [id]
        branch = get_children(branch, response_data, id)

        for child_id in branch:
            Model.Item.query.filter_by(id=child_id).delete(synchronize_session=False)
        db.session.commit()
        db.session.close()
        return {}, 204
