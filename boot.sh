#!/bin/sh

cd /home
python -m venv venv
./venv/bin/pip3 install -r requirements.txt
source venv/bin/activate
python migrate.py db migrate
python migrate.py db upgrade
python run.py
